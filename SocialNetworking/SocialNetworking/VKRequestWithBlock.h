//
//  VKRequestWithBlock.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "VKRequest.h"

typedef void (^VKRequestComplitionBlock)(id response, NSError *error);

@interface VKRequestWithBlock : VKRequest

@property (nonatomic, strong) VKRequestComplitionBlock block;

@end
