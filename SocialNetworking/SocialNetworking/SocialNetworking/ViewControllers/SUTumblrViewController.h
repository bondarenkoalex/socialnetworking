//
//  SUTumblrViewController.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SUTumblrViewController : UIViewController

- (IBAction)clientPostTextAction:(id)sender;
- (IBAction)apiPostTextAction:(id)sender;
- (IBAction)apiPostImageAction:(id)sender;

@end
