//
//  SUInstagramViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUInstagramViewController.h"

@interface SUInstagramViewController ()

@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;

@end

@implementation SUInstagramViewController

- (IBAction)postImage:(id)sender
{
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"image" withExtension:@"png"];
    
    self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
    self.documentInteractionController.UTI = @"com.instagram.photo";
    [self.documentInteractionController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
}

@end
