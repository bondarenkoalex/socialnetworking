//
//  SUVkontakteViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUVkontakteViewController.h"
#import "VKConnector.h"
#import "VKRequest.h"
#import "VKAccessToken.h"
#import "VKRequestManager.h"
#import "VKUser.h"
#import "VKRequestManagerWithBlocks.h"
#import "NSData+Base64.h"

static NSString *const kVKAppID = @"3941221";
static NSString *const kVKPermissionsArray = @"photos,friends,wall,audio,video,docs,notes,pages,status,groups,messages";

static NSString *const kSUAlbumName = @"SigmaTestAppClient3";

@interface SUVkontakteViewController ()<VKConnectorDelegate, VKRequestDelegate>

@property (nonatomic, strong) VKRequestManagerWithBlocks *requestManagerWithBlocks;
@property (nonatomic, strong) VKRequestManager *requestManager;

@end

@implementation SUVkontakteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[VKConnector sharedInstance] startWithAppID:kVKAppID
                                      permissons:[kVKPermissionsArray componentsSeparatedByString:@","]
                                        delegate:self];
    
    self.requestManagerWithBlocks = [[VKRequestManagerWithBlocks alloc] initWithDelegate:self user:[VKUser currentUser]];
}

- (IBAction)postTextAction:(id)sender
{
    [self postPostWithDictionary:@{@"message": @"test text"} withComplitionBlock:nil];
}

- (IBAction)postImageAction:(id)sender
{
    [self uploadImage:[UIImage imageNamed:@"image.png"] withComplitionBlock:^(NSString *imageId) {
    
        [self postPostWithDictionary:@{@"attachments": imageId, @"message": @"test text"} withComplitionBlock:^(id response, NSError *error) {
            
        }];
    }];
}

- (void)uploadImage:(UIImage *)image withComplitionBlock:(void(^)(NSString *imageId))block
{
    [self checkUploadServerWithComplitionBlock:^(id response, NSError *error) {
        
        [self.requestManagerWithBlocks postData:UIImageJPEGRepresentation(image, 0.5)
                                          toURL:[response valueForKeyPath:@"response.upload_url"]
                            withComplitionBlock:^(id response, NSError *error) {
                                
                                [self saveImageWithUploadResponse:response withComplitionBlock:^(id response, NSError *error) {
                                    if (block)
                                    {
                                        block([[response valueForKeyPath:@"response.id"] firstObject]);
                                    }
                                }];
                            }];
        
    }];
}

- (void)postPostWithDictionary:(NSDictionary *)dictionary withComplitionBlock:(VKRequestComplitionBlock)block
{
    VKRequestWithBlock *requestWithBlock = (VKRequestWithBlock *)[self.requestManagerWithBlocks wallPost:dictionary];
    requestWithBlock.block = block;
    [requestWithBlock start];
}

- (void)checkUploadServerWithComplitionBlock:(VKRequestComplitionBlock)block
{
    VKRequestWithBlock *requestWithBlock = (VKRequestWithBlock *)[self.requestManagerWithBlocks photosGetWallUploadServer:@{}];
    requestWithBlock.block = block;
    
    [requestWithBlock start];
}

- (void)saveImageWithUploadResponse:(id)response withComplitionBlock:(VKRequestComplitionBlock)block
{
    VKRequestWithBlock *requestWithBlock = (VKRequestWithBlock *)[self.requestManagerWithBlocks photosSaveWallPhoto:response];
    requestWithBlock.block = block;
    
    [requestWithBlock start];
}

#pragma mark VKRequestDelegate

- (void)VKRequest:(VKRequest *)request response:(id)response
{
    NSLog(@"%s", __FUNCTION__);
    NSLog(@"response: %@", response);
}

@end
