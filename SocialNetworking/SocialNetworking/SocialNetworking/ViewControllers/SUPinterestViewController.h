//
//  SUPinterestViewController.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SUPinterestViewController : UIViewController

- (IBAction)postAction:(id)sender;

@end
