//
//  SUTwitterViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUTwitterViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SUTwitterViewController ()

@property (nonatomic, strong) SLComposeViewController *composeViewController;

@end

@implementation SUTwitterViewController

- (IBAction)postTextAction:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        self.composeViewController = [[SLComposeViewController alloc] init];
        self.composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [self.composeViewController setInitialText:@"test text"];
        [self presentViewController:self.composeViewController animated:YES completion:nil];
    }
}

- (IBAction)postImage:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        self.composeViewController = [[SLComposeViewController alloc] init];
        self.composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [self.composeViewController addImage:[UIImage imageNamed:@"image.png"]];
        [self presentViewController:self.composeViewController animated:YES completion:nil];
    }
}

@end
