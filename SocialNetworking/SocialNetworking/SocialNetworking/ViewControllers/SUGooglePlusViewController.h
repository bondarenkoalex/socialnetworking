//
//  SUGooglePlusViewController.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SUGooglePlusViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

- (IBAction)postAction:(id)sender;
- (IBAction)loginAction:(id)sender;
- (IBAction)logoutAction:(id)sender;

@end
