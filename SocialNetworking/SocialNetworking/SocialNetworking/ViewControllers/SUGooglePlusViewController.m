//
//  SUGooglePlusViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUGooglePlusViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface SUGooglePlusViewController ()<GPPSignInDelegate, GPPShareDelegate>

@end

@implementation SUGooglePlusViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GPPSignIn sharedInstance].delegate = self;
    [GPPSignIn sharedInstance].actions = [NSArray arrayWithObjects:
                                          @"http://schemas.google.com/AddActivity",
                                          @"http://schemas.google.com/BuyActivity",
                                          @"http://schemas.google.com/CheckInActivity",
                                          @"http://schemas.google.com/CommentActivity",
                                          @"http://schemas.google.com/CreateActivity",
                                          @"http://schemas.google.com/ListenActivity",
                                          @"http://schemas.google.com/ReserveActivity",
                                          @"http://schemas.google.com/ReviewActivity",
                                          nil];
    BOOL isAuthenticated = [[GPPSignIn sharedInstance] trySilentAuthentication];
    [self updateStatusAuthenticated:isAuthenticated];
    
    [GPPShare sharedInstance].delegate = self;
}

- (void)updateStatusAuthenticated:(BOOL)authenticated
{
    if (authenticated)
    {
        self.statusLabel.text = @"authenticated";
    }
    else
    {
        self.statusLabel.text = @"not authenticated";
    }
}

- (IBAction)loginAction:(id)sender
{
    [self updateStatusAuthenticated:([GPPSignIn sharedInstance].authentication) ? YES : NO];
    
    if (![GPPSignIn sharedInstance].authentication)
    {
        [[GPPSignIn sharedInstance] authenticate];
    }
}

- (IBAction)logoutAction:(id)sender
{
    [[GPPSignIn sharedInstance] signOut];
    [self updateStatusAuthenticated:NO];
}

- (IBAction)postAction:(id)sender
{
    id<GPPShareBuilder> shareBuilder = [[GPPShare sharedInstance] shareDialog];
    [shareBuilder setContentDeepLinkID:@"deepLink"];
    [shareBuilder setTitle:@"Title"
               description:@"Description"
              thumbnailURL:[NSURL URLWithString:@"http://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Felis_silvestris_silvestris.jpg/265px-Felis_silvestris_silvestris.jpg"]];
    
    [shareBuilder open];
}

#pragma mark - GPPSignInDelegate

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    [self updateStatusAuthenticated:(error) ? NO : YES];
}

#pragma mark - GPPShareDelegate

- (void)finishedSharing:(BOOL)shared
{
    
}

@end
