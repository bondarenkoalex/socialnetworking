//
//  SUTumblrViewController.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/17/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "SUTumblrViewController.h"
#import "TMAPIClient.h"
#import "TMTumblrAppClient.h"

@interface SUTumblrViewController ()

@end

@implementation SUTumblrViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TMAPIClient sharedInstance].OAuthConsumerKey = @"V8Xn5KdVCJtOPiD63Z6vsGPrQgXlaayv8ksxErF1Xr2QgWrT48";
    [TMAPIClient sharedInstance].OAuthConsumerSecret = @"CiDWdC9PIxXdAoMJiyH4L2kgY7oVy8fr0WD1qYTONhEl3LCOrM";
}

- (IBAction)clientPostTextAction:(id)sender
{
    [TMTumblrAppClient createTextPost:@"Title" body:@"Body" tags:@[@"testTag"]];
}

- (IBAction)apiPostTextAction:(id)sender
{
    if (![TMAPIClient sharedInstance].OAuthToken)
    {
        [[TMAPIClient sharedInstance] authenticate:@"sigma.socialNetworking" callback:^(NSError *error) {
            [self postText];
            
        }];
    }
    else
    {
        [self postText];
    }
}

- (IBAction)apiPostImageAction:(id)sender
{
    if (![TMAPIClient sharedInstance].OAuthToken)
    {
        [[TMAPIClient sharedInstance] authenticate:@"sigma.socialNetworking" callback:^(NSError *error) {
            [self postPhoto];
            
        }];
    }
    else
    {
        [self postPhoto];
    }
}

- (void)postText
{
    [[TMAPIClient sharedInstance] userInfo:^(id info, NSError *error) {
        NSString *firstBlogName = [self firstBlogNameWithUserInfo:info];
        
        [[TMAPIClient sharedInstance] text:firstBlogName parameters:@{@"title": @"Titile", @"body": @"Body"} callback:^(id info, NSError *error) {
            NSLog(@"%@", error);
        }];
    }];
}

- (void)postPhoto
{
    [[TMAPIClient sharedInstance] userInfo:^(id info, NSError *error) {
        NSString *firstBlogName = [self firstBlogNameWithUserInfo:info];
        
        [[TMAPIClient sharedInstance] photo:firstBlogName
                              filePathArray:@[[[NSBundle mainBundle] pathForResource:@"image" ofType:@"png"]]
                           contentTypeArray:@[@"image/png"]
                              fileNameArray:@[@"image.png"]
                                 parameters:@{@"caption" : @"Caption"}
                                   callback:^(id response, NSError *error) {
                                       NSLog(@"%@", error);
                                   }];
    }];
}

- (NSString *)firstBlogNameWithUserInfo:(NSDictionary *)userInfo
{
    NSArray *blogs = [userInfo valueForKeyPath:@"user.blogs"];
    NSString *name = [[blogs objectAtIndex:0] valueForKeyPath:@"name"];
    return name;
}
@end
