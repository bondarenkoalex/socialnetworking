//
//  SUAppDelegate.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/15/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>

@interface SUAppDelegate : UIResponder <UIApplicationDelegate, GPPDeepLinkDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
