//
//  VKFakeRequestDelegate.h
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VKRequestWithBlock.h"

@interface VKFakeRequestDelegate : NSObject <VKRequestDelegate>

@property (nonatomic, weak) id <VKRequestDelegate> originDelegate;
@property (nonatomic, strong) VKRequestComplitionBlock complitionBlock;

@end
