//
//  VKRequestWithBlock.m
//  SocialNetworking
//
//  Created by Bondarenko Alexander on 10/21/13.
//  Copyright (c) 2013 Sigma. All rights reserved.
//

#import "VKRequestWithBlock.h"
#import "VKFakeRequestDelegate.h"

@interface VKRequestWithBlock()

@property (nonatomic, strong) VKFakeRequestDelegate *fakeRequestDelegate;

@end

@implementation VKRequestWithBlock

- (instancetype)initWithHTTPMethod:(NSString *)httpMethod
                               URL:(NSURL *)url
                           headers:(NSDictionary *)headers
                              body:(NSData *)body
{
    self = [super initWithHTTPMethod:httpMethod URL:url headers:headers body:body];
    
    if (self)
    {
        self.fakeRequestDelegate = [[VKFakeRequestDelegate alloc] init];
    }
    
    return self;
}
- (instancetype)initWithMethod:(NSString *)methodName
                       options:(NSDictionary *)options
{
    self = [super initWithMethod:methodName options:options];
    
    if (self)
    {
        self.fakeRequestDelegate = [[VKFakeRequestDelegate alloc] init];
    }

    return self;
}

- (void)setDelegate:(id<VKRequestDelegate>)delegate
{
    self.fakeRequestDelegate.originDelegate = delegate;
}

- (id<VKRequestDelegate>)delegate
{
    return self.fakeRequestDelegate;
}

- (void)setBlock:(VKRequestComplitionBlock)block
{
    _block = block;
    self.fakeRequestDelegate.complitionBlock = block;
}

@end
